let collection = [];

// Write the queue functions below.

function print() {

		return collection;
}

function enqueue(element) {

  collection.push(element);
  return collection;

}

function dequeue() {

	collection.shift();
	return collection;
}

function front() {

	return collection[0];
}

function size() {

	return collection.length;
}


function isEmpty() {

	// collection.length === 0;
	return collection.length === 0;
}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	isEmpty,
	size
};